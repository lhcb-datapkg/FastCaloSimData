# FastCalorimeterSimulation

Location of Fast Calorimeter Simulation data.

The nominal indexing of the data is done by
* Data Taking year
* Particle Type
 
So structure is for instance
`2016/photon`