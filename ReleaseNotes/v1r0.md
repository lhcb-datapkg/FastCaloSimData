2020-07-02 FastCaloSimData v1r0
========================================

This version is released on 'master' branch.

- Added point library for photons for 2016 conditions, see !2 (@mrama).

